Starsector Cooperative Combat Mod

https://bitbucket.org/NickWWest/starsectorcoopcombat

This mod allows any number of teammates to participate in combat with the main player and pilot their own ship. 
   
How this mod works:

A Cooperative AI replaces the standard ship AI when a player presses the "select next ship" key.
  This AI has separate keys bound to control its actions (configured in settings.json).  Ships behave per usual with the 
  following exceptions and solutions: 
  
 * No mouse to control where a shield faces - Use an AI to "aim" the shield
 * No mouse to control where weapons aim - Weapons can only aim directly forward
 * No mouse to select targets for weapons -  A new "cycle targets" command
 

Built using IntelliJ
  
License: MIT - Do what ever you want, I'm not liable