Starsector CoOperative Combat

Now your friends can help out in combat!

Features
* Experience combat with up to 3 friends
* New UI elements for additional players

How to use:
1. Download & install this mod
2. Make sure this mod is enabled
3. When combat starts and ships are deployed, have your friend press the Select Ship key ('Insert' by default for player 2)
4. During combat, have your friend use the keys specified in mods/CoOpCombat/Player2.json to pilot their ship (if you change the settings, you'll need to restart Starsector)
5. If sharing a keyboard is hard (or awkward), use a second keyboard or a game pad that can emulate keys.
6. When combat starts, zoom out more than usual so your friends show up on the screen
7. -optional- Install the CombatAnalytics mod (http://fractalsoftworks.com/forum/index.php?topic=11551.0) so you can see who's better.

Controller Support (optional)
* Use JoyToKey: https://joytokey.net/en/
* Have it emit/emulate the keys configured in step 4

Internet/Remote Play
Install a game streaming app (like Parsec.tv), you'll need a decent internet connection to pull this off.

Mod compatibility:
 Requires LunaLib, should work with any mod that doesn't assign very high priority AIs.

Key Configuration:
 To get the names of keys to use, look at: http://legacy.lwjgl.org/javadoc/org/lwjgl/input/Keyboard.html
 Just remove the 'KEY_' prefix

Known Issues/Limitations:
 * No mouse to control where a shield faces, instead an AI is used to "aim" the shield
 * No mouse to control where weapons aim, weapons can only aim directly forward.  Mitigate this by using in your group 1 only weapons that fire directly forward.
 * No mouse to select targets for weapons, instead a new "cycle targets" command has been added which cycles through nearby targets
 * Camera/viewport stays centered on Player1, to compensate zoom out level has been greatly increased.

What this mod is not and will not be (even though it might be cool):
* An MMO
* Allow you and a friend to both have different fleets on the campaign map

Using this mod in other mods:
This mod is covered by the MIT license, you can do what ever you want with this, no need to ask.  Just be nice and mention that this mod exists and where you got it from :)

Acknowledgements:
Some code related to rendering was taken from the excellent Leading Pip Mod, attribution is here by granted to Magehand LLC per licensing.


Change Log:
Version 3.8
 Updated for StarSector .96a
 Now uses LunaLib for key configuration

3.7
 * Updated for StarSector .95a

3.6
 * Players can now change which ship they are controlling during battle ('Insert' key by default for player 2)
 * Players can now retreat. Simply order a retreat and switch ships until no one is controlling any retreating ships.
 * Changed how keys are configured to be a bit simpler to add additional players
 * Backwards compatible

3.5
 * Made many values configurable in the CoOpCombatSettings.json
 * Increased lock-on range
 * Added engage/regroup toggle for carriers
 * Player 2+ aiming reticles are larger
 * Attempts at fixing Auto-Fire issue.

3.4
 * Add ability to toggle phase-cloak

3.3
 * prevent exception happening when transferring command

3.2
 * Updated for .9a

3.1
 * Add mission support, just edit the variant of a ship to be "player#" like you would with the ship name
 * Added experimental PvP mission (Doing anything more interesting would require work)

3.0
 * Updated for 8.1a

2.0 - UI elements for:
        * Selected weapon group
        * Weapon cooldown
        * System cooldown
        * Weapons have their own targeting lane
        * Switching between weapon groups now works (had to disable autofire)

1.0 - Released


Forum: http://fractalsoftworks.com/forum/index.php?topic=11598.0
Source: https://bitbucket.org/NickWWest/starsectorcoopcombat
License: MIT License (Do what ever you want, creators not liable)



