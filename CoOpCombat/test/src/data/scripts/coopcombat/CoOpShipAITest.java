package data.scripts.coopcombat;

import com.fs.starfarer.api.combat.ShipAPI;
import org.junit.Assert;
import org.junit.Test;
import org.lwjgl.util.vector.Vector2f;

public class CoOpShipAITest{

    @Test
    public void TestGetLocationInFront(){
        Vector2f v = CoOpShipAI.getLocationInFront(100, new Vector2f(12f, 13f), 90);

        Assert.assertEquals(12f, v.getX(), .000001);
        Assert.assertEquals(113f, v.getY(), .000001);

        v = CoOpShipAI.getLocationInFront(100, new Vector2f(12f, 13f), 0);

        Assert.assertEquals(112f, v.getX(), .000001);
        Assert.assertEquals(13f, v.getY(), .000001);
    }
}