package data.scripts.coopcombat;

import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.loading.*;

import java.util.*;

public class MockShipVariant implements ShipVariantAPI {
    @Override
    public ShipVariantAPI clone() {
        return null;
    }

    @Override
    public ShipHullSpecAPI getHullSpec() {
        return null;
    }

    public String displayName;

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getDesignation() {
        return null;
    }

    @Override
    public Collection<String> getHullMods() {
        return null;
    }

    @Override
    public void clearHullMods() {

    }

    @Override
    public EnumSet<ShipHullSpecAPI.ShipTypeHints> getHints() {
        return null;
    }

    @Override
    public void addMod(String s) {

    }

    @Override
    public void removeMod(String s) {

    }

    @Override
    public void addWeapon(String s, String s1) {

    }

    @Override
    public int getNumFluxVents() {
        return 0;
    }

    @Override
    public int getNumFluxCapacitors() {
        return 0;
    }

    @Override
    public List<String> getNonBuiltInWeaponSlots() {
        return null;
    }

    @Override
    public String getWeaponId(String s) {
        return null;
    }

    @Override
    public void setNumFluxCapacitors(int i) {

    }

    @Override
    public void setNumFluxVents(int i) {

    }

    @Override
    public void setSource(VariantSource variantSource) {

    }

    @Override
    public void clearSlot(String s) {

    }

    @Override
    public WeaponSpecAPI getWeaponSpec(String s) {
        return null;
    }

    @Override
    public Collection<String> getFittedWeaponSlots() {
        return null;
    }

    @Override
    public void autoGenerateWeaponGroups() {

    }

    @Override
    public boolean hasUnassignedWeapons() {
        return false;
    }

    @Override
    public void assignUnassignedWeapons() {

    }

    @Override
    public WeaponGroupSpec getGroup(int i) {
        return null;
    }

    @Override
    public int computeOPCost(MutableCharacterStatsAPI mutableCharacterStatsAPI) {
        return 0;
    }

    @Override
    public int computeWeaponOPCost(MutableCharacterStatsAPI mutableCharacterStatsAPI) {
        return 0;
    }

    @Override
    public int computeHullModOPCost() {
        return 0;
    }

    @Override
    public int computeHullModOPCost(MutableCharacterStatsAPI mutableCharacterStatsAPI) {
        return 0;
    }

    @Override
    public VariantSource getSource() {
        return null;
    }

    @Override
    public boolean isStockVariant() {
        return false;
    }

    @Override
    public boolean isEmptyHullVariant() {
        return false;
    }

    @Override
    public void setHullVariantId(String s) {

    }

    @Override
    public String getHullVariantId() {
        return null;
    }

    @Override
    public List<WeaponGroupSpec> getWeaponGroups() {
        return null;
    }

    @Override
    public void addWeaponGroup(WeaponGroupSpec weaponGroupSpec) {

    }

    @Override
    public void setVariantDisplayName(String s) {

    }

    @Override
    public ShipAPI.HullSize getHullSize() {
        return null;
    }

    @Override
    public boolean isFighter() {
        return false;
    }

    @Override
    public String getFullDesignationWithHullName() {
        return null;
    }

    @Override
    public boolean hasHullMod(String s) {
        return false;
    }

    @Override
    public WeaponSlotAPI getSlot(String s) {
        return null;
    }

    @Override
    public boolean isCombat() {
        return false;
    }

    @Override
    public boolean isStation() {
        return false;
    }

    @Override
    public String getWingId(int i) {
        return null;
    }

    @Override
    public void setWingId(int i, String s) {

    }

    @Override
    public List<String> getWings() {
        return null;
    }

    @Override
    public List<String> getLaunchBaysSlotIds() {
        return null;
    }

    @Override
    public List<String> getFittedWings() {
        return null;
    }

    @Override
    public void setHullSpecAPI(ShipHullSpecAPI shipHullSpecAPI) {

    }

    @Override
    public Set<String> getPermaMods() {
        return null;
    }

    @Override
    public void clearPermaMods() {

    }

    @Override
    public void removePermaMod(String s) {

    }

    @Override
    public void addPermaMod(String s) {

    }

    @Override
    public void addPermaMod(String modId, boolean isSMod) {

    }

    @Override
    public boolean isCarrier() {
        return false;
    }

    @Override
    public List<String> getSortedMods() {
        return null;
    }

    @Override
    public Set<String> getSuppressedMods() {
        return null;
    }

    @Override
    public void addSuppressedMod(String s) {

    }

    @Override
    public void removeSuppressedMod(String s) {

    }

    @Override
    public void clearSuppressedMods() {

    }

    @Override
    public boolean isGoalVariant() {
        return false;
    }

    @Override
    public void setGoalVariant(boolean b) {

    }

    @Override
    public Collection<String> getNonBuiltInHullmods() {
        return null;
    }

    @Override
    public FighterWingSpecAPI getWing(int i) {
        return null;
    }

    @Override
    public int getUnusedOP(MutableCharacterStatsAPI mutableCharacterStatsAPI) {
        return 0;
    }

    @Override
    public boolean isCivilian() {
        return false;
    }

    @Override
    public List<String> getModuleSlots() {
        return null;
    }

    @Override
    public MutableShipStatsAPI getStatsForOpCosts() {
        return null;
    }

    @Override
    public boolean isLiner() {
        return false;
    }

    @Override
    public boolean isFreighter() {
        return false;
    }

    @Override
    public boolean isTanker() {
        return false;
    }

    @Override
    public boolean isDHull() {
        return false;
    }

    @Override
    public Map<String, String> getStationModules() {
        return null;
    }

    @Override
    public List<String> getNonBuiltInWings() {
        return null;
    }

    @Override
    public boolean hasTag(String s) {
        return false;
    }

    @Override
    public void addTag(String s) {

    }

    @Override
    public void removeTag(String s) {

    }

    @Override
    public Collection<String> getTags() {
        return null;
    }

    @Override
    public void clearTags() {

    }

    @Override
    public void clear() {

    }

    @Override
    public String getOriginalVariant() {
        return "original";
    }

    @Override
    public void setOriginalVariant(String targetVariant) {

    }

    @Override
    public ShipVariantAPI getModuleVariant(String slotId) {
        return null;
    }

    @Override
    public void setModuleVariant(String slotId, ShipVariantAPI variant) {

    }

    @Override
    public boolean isTransport() {
        return false;
    }

    @Override
    public String getVariantFilePath() {
        return null;
    }

    @Override
    public LinkedHashSet<String> getSMods() {
        return null;
    }

    @Override
    public String getFullDesignationWithHullNameForShip() {
        return null;
    }

    @Override
    public void refreshBuiltInWings() {

    }

    @Override
    public boolean hasDMods() {
        return false;
    }

    @Override
    public LinkedHashSet<String> getSModdedBuiltIns() {
        return null;
    }
}
