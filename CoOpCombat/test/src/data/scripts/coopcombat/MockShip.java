package data.scripts.coopcombat;

import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.combat.listeners.CombatListenerManagerAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MockShip implements ShipAPI {
    @Override
    public String getFleetMemberId() {
        return null;
    }

    @Override
    public Vector2f getMouseTarget() {
        return null;
    }

    @Override
    public boolean isShuttlePod() {
        return false;
    }

    @Override
    public boolean isDrone() {
        return false;
    }

    @Override
    public boolean isFighter() {
        return false;
    }

    @Override
    public boolean isFrigate() {
        return false;
    }

    @Override
    public boolean isDestroyer() {
        return false;
    }

    @Override
    public boolean isCruiser() {
        return false;
    }

    @Override
    public boolean isCapital() {
        return false;
    }

    @Override
    public HullSize getHullSize() {
        return null;
    }

    @Override
    public ShipAPI getShipTarget() {
        return null;
    }

    @Override
    public void setShipTarget(ShipAPI shipAPI) {

    }

    @Override
    public int getOriginalOwner() {
        return 0;
    }

    @Override
    public void setOriginalOwner(int i) {

    }

    @Override
    public void resetOriginalOwner() {

    }

    @Override
    public MutableShipStatsAPI getMutableStats() {
        return null;
    }

    @Override
    public boolean isHulk() {
        return false;
    }

    @Override
    public List<WeaponAPI> getAllWeapons() {
        return null;
    }

    @Override
    public ShipSystemAPI getPhaseCloak() {
        return null;
    }

    @Override
    public ShipSystemAPI getSystem() {
        return null;
    }

    @Override
    public ShipSystemAPI getTravelDrive() {
        return null;
    }

    @Override
    public void toggleTravelDrive() {

    }

    @Override
    public void setShield(ShieldAPI.ShieldType shieldType, float v, float v1, float v2) {

    }

    @Override
    public ShipHullSpecAPI getHullSpec() {
        return null;
    }


    MockShipVariant variant;
    @Override
    public ShipVariantAPI getVariant() {
        return variant;
    }

    @Override
    public void useSystem() {

    }

    @Override
    public FluxTrackerAPI getFluxTracker() {
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public List<ShipAPI> getWingMembers() {
        return null;
    }

    @Override
    public ShipAPI getWingLeader() {
        return null;
    }

    @Override
    public boolean isWingLeader() {
        return false;
    }

    @Override
    public FighterWingAPI getWing() {
        return null;
    }

    @Override
    public List<ShipAPI> getDeployedDrones() {
        return null;
    }

    @Override
    public ShipAPI getDroneSource() {
        return null;
    }

    @Override
    public Object getWingToken() {
        return null;
    }

    @Override
    public ArmorGridAPI getArmorGrid() {
        return null;
    }

    @Override
    public void setRenderBounds(boolean b) {

    }

    @Override
    public void setCRAtDeployment(float v) {

    }

    @Override
    public float getCRAtDeployment() {
        return 0;
    }

    @Override
    public float getCurrentCR() {
        return 0;
    }

    @Override
    public void setCurrentCR(float v) {

    }

    @Override
    public float getWingCRAtDeployment() {
        return 0;
    }

    @Override
    public void setHitpoints(float v) {

    }

    @Override
    public float getTimeDeployedForCRReduction() {
        return 0;
    }

    @Override
    public float getFullTimeDeployed() {
        return 0;
    }

    @Override
    public boolean losesCRDuringCombat() {
        return false;
    }

    @Override
    public boolean controlsLocked() {
        return false;
    }

    @Override
    public void setControlsLocked(boolean b) {

    }

    @Override
    public void setShipSystemDisabled(boolean b) {

    }

    @Override
    public Set<WeaponAPI> getDisabledWeapons() {
        return null;
    }

    @Override
    public int getNumFlameouts() {
        return 0;
    }

    @Override
    public float getHullLevelAtDeployment() {
        return 0;
    }

    @Override
    public void setSprite(String s, String s1) {

    }

    @Override
    public SpriteAPI getSpriteAPI() {
        return null;
    }

    @Override
    public ShipEngineControllerAPI getEngineController() {
        return null;
    }

    @Override
    public void giveCommand(ShipCommand shipCommand, Object o, int i) {

    }

    @Override
    public void setShipAI(ShipAIPlugin shipAIPlugin) {

    }

    @Override
    public ShipAIPlugin getShipAI() {
        return null;
    }

    @Override
    public void resetDefaultAI() {

    }

    @Override
    public void turnOnTravelDrive() {

    }

    @Override
    public void turnOnTravelDrive(float v) {

    }

    @Override
    public void turnOffTravelDrive() {

    }

    @Override
    public boolean isRetreating() {
        return false;
    }


    @Override
    public void abortLanding() {

    }

    @Override
    public void beginLandingAnimation(ShipAPI shipAPI) {

    }

    @Override
    public boolean isLanding() {
        return false;
    }

    @Override
    public boolean isFinishedLanding() {
        return false;
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public boolean isInsideNebula() {
        return false;
    }

    @Override
    public void setInsideNebula(boolean b) {

    }

    @Override
    public boolean isAffectedByNebula() {
        return false;
    }

    @Override
    public void setAffectedByNebula(boolean b) {

    }

    @Override
    public float getDeployCost() {
        return 0;
    }

    @Override
    public void removeWeaponFromGroups(WeaponAPI weaponAPI) {

    }

    @Override
    public void applyCriticalMalfunction(Object o) {

    }

    @Override
    public float getBaseCriticalMalfunctionDamage() {
        return 0;
    }

    @Override
    public float getEngineFractionPermanentlyDisabled() {
        return 0;
    }

    @Override
    public float getCombinedAlphaMult() {
        return 0;
    }

    @Override
    public float getLowestHullLevelReached() {
        return 0;
    }

    @Override
    public ShipwideAIFlags getAIFlags() {
        return null;
    }

    @Override
    public List<WeaponGroupAPI> getWeaponGroupsCopy() {
        return null;
    }

    @Override
    public boolean isHoldFire() {
        return false;
    }

    @Override
    public boolean isHoldFireOneFrame() {
        return false;
    }

    @Override
    public void setHoldFireOneFrame(boolean b) {

    }

    @Override
    public boolean isPhased() {
        return false;
    }

    @Override
    public boolean isAlly() {
        return false;
    }

    @Override
    public void setWeaponGlow(float v, Color color, EnumSet<WeaponAPI.WeaponType> enumSet) {

    }

    @Override
    public void setVentCoreColor(Color color) {

    }

    @Override
    public void setVentFringeColor(Color color) {

    }

    @Override
    public Color getVentCoreColor() {
        return null;
    }

    @Override
    public Color getVentFringeColor() {
        return null;
    }

    @Override
    public String getHullStyleId() {
        return null;
    }

    @Override
    public WeaponGroupAPI getWeaponGroupFor(WeaponAPI weaponAPI) {
        return null;
    }

    @Override
    public void setCopyLocation(Vector2f vector2f, float v, float v1) {

    }

    @Override
    public Vector2f getCopyLocation() {
        return null;
    }

    @Override
    public void setAlly(boolean b) {

    }

    @Override
    public void applyCriticalMalfunction(Object o, boolean b) {

    }

    @Override
    public String getId() {
        return null;
    }



    public String name;
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setJitter(Object o, Color color, float v, int i, float v1) {

    }

    @Override
    public void setJitterUnder(Object o, Color color, float v, int i, float v1) {

    }

    @Override
    public void setJitter(Object o, Color color, float v, int i, float v1, float v2) {

    }

    @Override
    public void setJitterUnder(Object o, Color color, float v, int i, float v1, float v2) {

    }

    @Override
    public float getTimeDeployedUnderPlayerControl() {
        return 0;
    }

    @Override
    public SpriteAPI getSmallTurretCover() {
        return null;
    }

    @Override
    public SpriteAPI getSmallHardpointCover() {
        return null;
    }

    @Override
    public SpriteAPI getMediumTurretCover() {
        return null;
    }

    @Override
    public SpriteAPI getMediumHardpointCover() {
        return null;
    }

    @Override
    public SpriteAPI getLargeTurretCover() {
        return null;
    }

    @Override
    public SpriteAPI getLargeHardpointCover() {
        return null;
    }

    @Override
    public boolean isDefenseDisabled() {
        return false;
    }

    @Override
    public void setDefenseDisabled(boolean b) {

    }

    @Override
    public void setPhased(boolean b) {

    }

    @Override
    public void setExtraAlphaMult(float v) {

    }

    @Override
    public void setApplyExtraAlphaToEngines(boolean b) {

    }

    @Override
    public void setOverloadColor(Color color) {

    }

    @Override
    public void resetOverloadColor() {

    }

    @Override
    public Color getOverloadColor() {
        return null;
    }

    @Override
    public boolean isRecentlyShotByPlayer() {
        return false;
    }

    @Override
    public float getMaxSpeedWithoutBoost() {
        return 0;
    }

    @Override
    public float getHardFluxLevel() {
        return 0;
    }

    @Override
    public void fadeToColor(Object o, Color color, float v, float v1, float v2) {

    }

    @Override
    public boolean isShowModuleJitterUnder() {
        return false;
    }

    @Override
    public void setShowModuleJitterUnder(boolean b) {

    }

    @Override
    public void addAfterimage(Color color, float v, float v1, float v2, float v3, float v4, float v5, float v6, float v7, boolean b, boolean b1, boolean b2) {

    }

    @Override
    public PersonAPI getCaptain() {
        return null;
    }

    @Override
    public WeaponSlotAPI getStationSlot() {
        return null;
    }

    @Override
    public void setStationSlot(WeaponSlotAPI weaponSlotAPI) {

    }

    @Override
    public ShipAPI getParentStation() {
        return null;
    }

    @Override
    public void setParentStation(ShipAPI shipAPI) {

    }

    @Override
    public Vector2f getFixedLocation() {
        return null;
    }

    @Override
    public void setFixedLocation(Vector2f vector2f) {

    }

    @Override
    public boolean hasRadarRibbonIcon() {
        return false;
    }

    @Override
    public boolean isTargetable() {
        return false;
    }

    @Override
    public void setStation(boolean b) {

    }

    @Override
    public boolean isSelectableInWarroom() {
        return false;
    }

    @Override
    public boolean isShipWithModules() {
        return false;
    }

    @Override
    public void setShipWithModules(boolean b) {

    }

    @Override
    public List<ShipAPI> getChildModulesCopy() {
        return null;
    }

    @Override
    public boolean isPiece() {
        return false;
    }

    @Override
    public BoundsAPI getVisualBounds() {
        return null;
    }

    @Override
    public Vector2f getRenderOffset() {
        return null;
    }

    @Override
    public ShipAPI splitShip() {
        return null;
    }

    @Override
    public int getNumFighterBays() {
        return 0;
    }

    @Override
    public boolean isPullBackFighters() {
        return false;
    }

    @Override
    public void setPullBackFighters(boolean b) {

    }

    @Override
    public boolean hasLaunchBays() {
        return false;
    }

    @Override
    public List<FighterLaunchBayAPI> getLaunchBaysCopy() {
        return null;
    }

    @Override
    public float getFighterTimeBeforeRefit() {
        return 0;
    }

    @Override
    public void setFighterTimeBeforeRefit(float v) {

    }

    @Override
    public List<FighterWingAPI> getAllWings() {
        return null;
    }

    @Override
    public float getSharedFighterReplacementRate() {
        return 0;
    }

    @Override
    public boolean areSignificantEnemiesInRange() {
        return false;
    }

    @Override
    public List<WeaponAPI> getUsableWeapons() {
        return null;
    }

    @Override
    public Vector2f getModuleOffset() {
        return null;
    }

    @Override
    public float getMassWithModules() {
        return 0;
    }

    @Override
    public PersonAPI getOriginalCaptain() {
        return null;
    }

    @Override
    public boolean isRenderEngines() {
        return false;
    }

    @Override
    public void setRenderEngines(boolean b) {

    }

    @Override
    public WeaponGroupAPI getSelectedGroupAPI() {
        return null;
    }

    @Override
    public void setHullSize(HullSize hullSize) {

    }

    @Override
    public void ensureClonedStationSlotSpec() {

    }

    @Override
    public void setMaxHitpoints(float maxArmor) {

    }

    @Override
    public void setDHullOverlay(String spriteName) {

    }

    @Override
    public boolean isStation() {
        return false;
    }

    @Override
    public boolean isStationModule() {
        return false;
    }

    @Override
    public boolean areAnyEnemiesInRange() {
        return false;
    }

    @Override
    public void blockCommandForOneFrame(ShipCommand command) {

    }

    @Override
    public float getMaxTurnRate() {
        return 0;
    }

    @Override
    public float getTurnAcceleration() {
        return 0;
    }

    @Override
    public float getTurnDeceleration() {
        return 0;
    }

    @Override
    public float getDeceleration() {
        return 0;
    }

    @Override
    public float getAcceleration() {
        return 0;
    }

    @Override
    public float getMaxSpeed() {
        return 0;
    }

    @Override
    public float getFluxLevel() {
        return 0;
    }

    @Override
    public float getCurrFlux() {
        return 0;
    }

    @Override
    public float getMaxFlux() {
        return 0;
    }

    @Override
    public float getMinFluxLevel() {
        return 0;
    }

    @Override
    public float getMinFlux() {
        return 0;
    }

    @Override
    public void setLightDHullOverlay() {

    }

    @Override
    public void setMediumDHullOverlay() {

    }

    @Override
    public void setHeavyDHullOverlay() {

    }

    @Override
    public boolean isJitterShields() {
        return false;
    }

    @Override
    public void setJitterShields(boolean jitterShields) {

    }

    @Override
    public boolean isInvalidTransferCommandTarget() {
        return false;
    }

    @Override
    public void setInvalidTransferCommandTarget(boolean invalidTransferCommandTarget) {

    }

    @Override
    public void clearDamageDecals() {

    }

    @Override
    public void syncWithArmorGridState() {

    }

    @Override
    public void syncWeaponDecalsWithArmorDamage() {

    }

    @Override
    public boolean isDirectRetreat() {
        return false;
    }

    @Override
    public void setRetreating(boolean retreating, boolean direct) {

    }

    @Override
    public boolean isLiftingOff() {
        return false;
    }

    @Override
    public void setVariantForHullmodCheckOnly(ShipVariantAPI variant) {

    }

    @Override
    public Vector2f getShieldCenterEvenIfNoShield() {
        return null;
    }

    @Override
    public float getShieldRadiusEvenIfNoShield() {
        return 0;
    }

    @Override
    public FleetMemberAPI getFleetMember() {
        return null;
    }

    @Override
    public Vector2f getShieldTarget() {
        return null;
    }

    @Override
    public void setShieldTargetOverride(float x, float y) {

    }

    @Override
    public CombatListenerManagerAPI getListenerManager() {
        return null;
    }

    @Override
    public void addListener(Object listener) {

    }

    @Override
    public void removeListener(Object listener) {

    }

    @Override
    public void removeListenerOfClass(Class<?> c) {

    }

    @Override
    public boolean hasListener(Object listener) {
        return false;
    }

    @Override
    public boolean hasListenerOfClass(Class<?> c) {
        return false;
    }

    @Override
    public <T> List<T> getListeners(Class<T> c) {
        return null;
    }

    @Override
    public Object getParamAboutToApplyDamage() {
        return null;
    }

    @Override
    public void setParamAboutToApplyDamage(Object param) {

    }

    @Override
    public float getFluxBasedEnergyWeaponDamageMultiplier() {
        return 0;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setHulk(boolean isHulk) {

    }

    @Override
    public void setCaptain(PersonAPI captain) {

    }

    @Override
    public float getShipExplosionRadius() {
        return 0;
    }

    @Override
    public void setCircularJitter(boolean circular) {

    }

    @Override
    public float getExtraAlphaMult() {
        return 0;
    }

    @Override
    public void setAlphaMult(float alphaMult) {

    }

    @Override
    public float getAlphaMult() {
        return 0;
    }

    @Override
    public void setAnimatedLaunch() {

    }

    @Override
    public void setLaunchingShip(ShipAPI launchingShip) {

    }

    @Override
    public boolean isNonCombat(boolean considerOrders) {
        return false;
    }

    @Override
    public float findBestArmorInArc(float facing, float arc) {
        return 0;
    }

    @Override
    public float getAverageArmorInSlice(float direction, float arc) {
        return 0;
    }

    @Override
    public void setHoldFire(boolean holdFire) {

    }

    @Override
    public void cloneVariant() {

    }

    @Override
    public void setTimeDeployed(float timeDeployed) {

    }

    @Override
    public void setFluxVentTextureSheet(String textureId) {

    }

    @Override
    public String getFluxVentTextureSheet() {
        return null;
    }

    @Override
    public float getAimAccuracy() {
        return 0;
    }

    @Override
    public float getForceCarrierTargetTime() {
        return 0;
    }

    @Override
    public void setForceCarrierTargetTime(float forceCarrierTargetTime) {

    }

    @Override
    public float getForceCarrierPullBackTime() {
        return 0;
    }

    @Override
    public void setForceCarrierPullBackTime(float forceCarrierPullBackTime) {

    }

    @Override
    public ShipAPI getForceCarrierTarget() {
        return null;
    }

    @Override
    public void setForceCarrierTarget(ShipAPI forceCarrierTarget) {

    }

    @Override
    public void setWing(FighterWingAPI wing) {

    }

    @Override
    public float getExplosionScale() {
        return 0;
    }

    @Override
    public void setExplosionScale(float explosionScale) {

    }

    @Override
    public Color getExplosionFlashColorOverride() {
        return null;
    }

    @Override
    public void setExplosionFlashColorOverride(Color explosionFlashColorOverride) {

    }

    @Override
    public Vector2f getExplosionVelocityOverride() {
        return null;
    }

    @Override
    public void setExplosionVelocityOverride(Vector2f explosionVelocityOverride) {

    }

    @Override
    public void setNextHitHullDamageThresholdMult(float threshold, float multBeyondThreshold) {

    }

    @Override
    public boolean isEngineBoostActive() {
        return false;
    }

    @Override
    public void makeLookDisabled() {

    }

    @Override
    public void setExtraAlphaMult2(float transparency) {

    }

    @Override
    public float getExtraAlphaMult2() {
        return 0;
    }

    @Override
    public void setDrone(boolean isDrone) {

    }

    @Override
    public CombatEngineLayers getLayer() {
        return null;
    }

    @Override
    public void setLayer(CombatEngineLayers layer) {

    }

    @Override
    public boolean isForceHideFFOverlay() {
        return false;
    }

    @Override
    public void setForceHideFFOverlay(boolean forceHideFFOverlay) {

    }

    @Override
    public Set<String> getTags() {
        return null;
    }

    @Override
    public void addTag(String tag) {

    }

    @Override
    public boolean hasTag(String tag) {
        return false;
    }

    @Override
    public void setSprite(SpriteAPI sprite) {

    }

    @Override
    public float getPeakTimeRemaining() {
        return 0;
    }

    @Override
    public EnumSet<CombatEngineLayers> getActiveLayers() {
        return null;
    }

    @Override
    public Vector2f getLocation() {
        return null;
    }

    @Override
    public Vector2f getVelocity() {
        return null;
    }

    @Override
    public float getFacing() {
        return 0;
    }

    @Override
    public void setFacing(float v) {

    }

    @Override
    public float getAngularVelocity() {
        return 0;
    }

    @Override
    public void setAngularVelocity(float v) {

    }

    @Override
    public int getOwner() {
        return 0;
    }

    @Override
    public void setOwner(int i) {

    }

    @Override
    public float getCollisionRadius() {
        return 0;
    }

    @Override
    public CollisionClass getCollisionClass() {
        return null;
    }

    @Override
    public void setCollisionClass(CollisionClass collisionClass) {

    }

    @Override
    public float getMass() {
        return 0;
    }

    @Override
    public void setMass(float v) {

    }

    @Override
    public BoundsAPI getExactBounds() {
        return null;
    }

    @Override
    public ShieldAPI getShield() {
        return null;
    }

    @Override
    public float getHullLevel() {
        return 0;
    }

    @Override
    public float getHitpoints() {
        return 0;
    }

    @Override
    public float getMaxHitpoints() {
        return 0;
    }

    @Override
    public void setCollisionRadius(float v) {

    }

    @Override
    public Object getAI() {
        return null;
    }

    @Override
    public boolean isExpired() {
        return false;
    }

    @Override
    public void setCustomData(String key, Object data) {

    }

    @Override
    public void removeCustomData(String key) {

    }

    @Override
    public Map<String, Object> getCustomData() {
        return null;
    }
}
