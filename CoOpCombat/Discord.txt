Fight battles with your friends via couch coop or internet play with streaming

**Cooperative Multiplayer Combat v3.8**
```
 - Backwards compatible
 - Updated to .96a
 - Luna Lib integration
 ```

 Download: https://bitbucket.org/NickWWest/starsectorcoopcombat/downloads/CoOpCombat.3.8.zip
 Forum Thread: http://fractalsoftworks.com/forum/index.php?topic=11598.0