package data.scripts.coopcombat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

// taken from the excellent Leading Pip Mod, attribution is here by granted to Magehand LLC per licensing.

/**
 * Given a ship, will draw a box around it indicating that the specific ship is "targeted" by a player
 * (missiles and beams will priority attack)
 */
public final class TargetedShip {
    private static final float ALPHA = .6f;
    private static final float THICKNESS = 1.5f;


    private static void glColor(Color color, float alphaMult) {
        GL11.glColor4ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue(), (byte) (color.getAlpha() * alphaMult));
    }

    public static void drawTargetedShip(ShipAPI ship, CombatEngineAPI engine, Color color) {
        if (engine == null || engine.getCombatUI() == null) {
            return;
        }

        if (!engine.isUIShowingHUD()) {
            return;
        }
        if (engine.getCombatUI().isShowingCommandUI()) {
            return;
        }

        if (ship == null) {
            return;
        }

        ViewportAPI viewport = engine.getViewport();

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        final int width = (int) (Display.getWidth() * Display.getPixelScaleFactor()), height = (int) (Display.getHeight() * Display.getPixelScaleFactor());
        GL11.glViewport(0, 0, width, height);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(viewport.getLLX(), viewport.getLLX() + viewport.getVisibleWidth(), viewport.getLLY(), viewport.getLLY() + viewport.getVisibleHeight(), -1, 1);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslatef(0.01f, 0.01f, 0);

        GL11.glLineWidth(THICKNESS);

        glColor(color, ALPHA * (1f - Global.getCombatEngine().getCombatUI().getCommandUIOpacity()));
        Vector2f center = ship.getLocation();
        float radius = ship.getCollisionRadius();

        GL11.glBegin(GL11.GL_LINE_LOOP);
        GL11.glVertex2f(center.x - radius, center.y + radius);
        GL11.glVertex2f(center.x + radius, center.y + radius);
        GL11.glVertex2f(center.x + radius, center.y - radius);
        GL11.glVertex2f(center.x - radius, center.y - radius);
        GL11.glEnd();

        GL11.glDisable(GL11.GL_BLEND);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();

        GL11.glPopAttrib();
    }
}
