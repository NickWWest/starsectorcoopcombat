/settings/ Code for the parsing and validation of external configuration (JSON)
/util/

CoOpCombatModPlugin ensures that a new CoOpEveryFrameCombatPlugin is loaded for each combat.  CoOpCombatModPlugin watches
for keypresses to enable the CoOpShipAI.  The CoOpShipAI watches for key presses every frame that get translated into
ship actions.