package data.scripts.coopcombat.util;

import com.fs.starfarer.api.combat.ShipCommand;
import org.lwjgl.input.Keyboard;

// simple wrapper simplifies checking for keypress events
public class KeyToCommand{
    public final int keyIndex;
    public final ShipCommand shipCommand;

    private boolean wasPressed;
    private final boolean toggleBehavior;

    public KeyToCommand(int keyIndex, ShipCommand shipCommand, boolean isToggle) {
        this.keyIndex = keyIndex;
        this.shipCommand = shipCommand;
        this.toggleBehavior = isToggle;
    }

    public KeyToCommand(int keyIndex, boolean isToggle) {
        this.keyIndex = keyIndex;
        this.shipCommand = null;
        this.toggleBehavior = isToggle;
    }

    public String toString(){
        return String.format("Key: '%1$s'  Command: '%2$s'", keyIndex, shipCommand == null ? "" : shipCommand.toString());
    }

    public boolean shouldGiveCommand(){
        boolean keyDown = Keyboard.isKeyDown(keyIndex);
        boolean ret = keyDown;

        // toggle behavior means we only care the first time we see you pressed (you have to be depressed before you cna be active again)
        if(toggleBehavior && keyDown && wasPressed){
            ret = false;
        }

        wasPressed = keyDown;
        return ret;
    }

}
