package data.scripts.coopcombat.util;

import com.fs.starfarer.api.Global;
import data.scripts.coopcombat.ConsoleCampaignListener;
import org.lwjgl.util.vector.Vector2f;

import java.awt.Color;

public class Helpers {
    public static void logCombatErrorMessage(String message){
        Global.getCombatEngine().getCombatUI().addMessage(1, Color.RED, message);
    }

    public static void printErrorMessage(String message){
        ConsoleCampaignListener.enqueueForLogging("CoOp Combat: "+message+"  See log for details.", Color.RED);
    }

    @SafeVarargs
    public static <T> T coalesce(T... values){

        for(T value : values){
            if(value != null){
                return value;
            }
        }

        return null;
    }

    // squared distance is good enough for ordering
    public static float getDistanceSquared(Vector2f v1, Vector2f v2){
        float deltaX = v1.x - v2.x;
        float deltaY = v1.y - v2.y;

        return (deltaX * deltaX) + (deltaY * deltaY);
    }

}
