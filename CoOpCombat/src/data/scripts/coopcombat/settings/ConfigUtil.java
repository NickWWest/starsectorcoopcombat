package data.scripts.coopcombat.settings;

import com.fs.starfarer.api.Global;
import data.scripts.coopcombat.util.Helpers;
import lunalib.lunaSettings.LunaSettings;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.awt.*;

class ConfigUtil {

    private static final Logger log = Global.getLogger(ConfigUtil.class);

    public static int getLunaInt(String setting){
        Integer ret = LunaSettings.getInt("CoOpCombat", setting);
        if(ret == null){
            String message = "Unable to locate the LunaConfig setting '"+setting+"'";
            log.error(message);
            throw new RuntimeException(message);
        }

        return (int)ret;
    }

    public static float getLunaFloat(String setting){
        Double ret = LunaSettings.getDouble("CoOpCombat", setting);
        if(ret == null){
            String message = "Unable to locate the LunaConfig setting '"+setting+"'";
            log.error(message);
            throw new RuntimeException(message);
        }

        return (float)(double)ret;
    }

    public static String getStringSetting(JSONObject settingsJson, String setting, String defaultValue){
        String ret = defaultValue;
        try {
            ret = settingsJson.getString(setting);
        } catch (Throwable e) {
            String message = "Unable to locate the setting '"+setting+"' in JSON, defaulting to '"+defaultValue+"'";
            log.error(message);
            Helpers.printErrorMessage(message);
        }

        return Helpers.coalesce(ret, defaultValue);
    }

    public static float getFloatSetting(JSONObject settingsJson, String setting, float defaultValue){
        float ret = defaultValue;
        try {
            ret = (float)settingsJson.getDouble(setting);
        } catch (Throwable e) {
            String message = "Unable to locate the setting '"+setting+"' in JSON, defaulting to '"+defaultValue+"'";
            log.error(message);
            Helpers.printErrorMessage(message);
        }

        return ret;
    }

    public static boolean getBooleanSetting(JSONObject settingsJson, String setting, boolean defaultValue){
        boolean ret = defaultValue;
        try {
            ret = settingsJson.getBoolean(setting);
        } catch (Throwable e) {
            String message = "Unable to locate the setting '"+setting+"' in JSON, defaulting to '"+defaultValue+"'";
            log.error(message);
            Helpers.printErrorMessage(message);
        }

        return ret;
    }

    public static int getIntegerSetting(JSONObject settingsJson, String setting, int defaultValue){
        int ret = defaultValue;
        try {
            ret = settingsJson.getInt(setting);
        } catch (Throwable e) {
            String message = "Unable to locate the setting '"+setting+"' in JSON, defaulting to '"+defaultValue+"'";
            log.error(message);
            Helpers.printErrorMessage(message);
        }

        return ret;
    }

    public static Color getColorSetting(JSONObject settingsJson, String setting, Color defaultValue){
        try {
            String rawColor = settingsJson.getString(setting);
            String[] color = rawColor.split("\\s*,\\s*", -1);
            return new Color(Integer.parseInt(color[0]), Integer.parseInt(color[1]), Integer.parseInt(color[2]));
        }
        catch (Throwable t){
            String message = "Unable to locate a valid setting for '"+setting+"' in JSON, defaulting to '"+defaultValue+"'";
            log.warn(message);
        }

        return defaultValue;
    }
}
