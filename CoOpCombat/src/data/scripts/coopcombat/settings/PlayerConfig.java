package data.scripts.coopcombat.settings;

import data.scripts.coopcombat.ConsoleCampaignListener;
import lunalib.lunaSettings.LunaSettings;
import org.json.JSONObject;

import java.awt.Color;
import java.util.*;

// player level "PlayerConfig" object, found inside of CoOpCombatSettings.json
public class PlayerConfig {

    private static final List<String> ConfiguredActions = Arrays.asList(
            "TURN_LEFT",
            "TURN_RIGHT",
            "STRAFE_LEFT",
            "STRAFE_RIGHT",
            "ACCELERATE",
            "ACCELERATE_BACKWARDS",
            "DECELERATE",
            "FIRE",
            "SELECT_GROUP",
            "TOGGLE_AUTOFIRE",
            "VENT_FLUX",
            "TOGGLE_SHIELD_OR_PHASE_CLOAK",
            "HOLD_FIRE",
            "USE_SYSTEM",
            "NEXT_TARGET",
            "PREV_TARGET",
            "PULL_BACK_FIGHTERS",
            "SELECT_SHIP"
    );

    // default colors if none is specified
    private static final Color[] playerColors = new Color[]{
            new Color(0, 242, 240),
            new Color(251, 196, 0),
            new Color(240, 255, 3),
    };

    public final int playerId;
    public final Color playerColor;
    public final Map<String, ActionKeyIndex> actionToKey = new HashMap<>();

    public PlayerConfig(int playerId) throws Exception {
        this.playerId = playerId;

        // get our player color if specified
        playerColor = playerColors[(playerId - 2) & playerColors.length];

        // setup & validate keybinds
        for(String key : ConfiguredActions){
            Integer keyIndex = getKeyCode(playerId, key);
            if(keyIndex == null){
                throw new Exception("Could not locate keyboard key for action: '"+key+"' for PlayerId: " + playerId);
            }
            actionToKey.put(key, new ActionKeyIndex(key, keyIndex));
        }
    }

    public Integer getKeyCode(int playerId, String setting) {
        String settingName = "p"+playerId+"_"+setting;
        return LunaSettings.getInt("CoOpCombat", settingName);
    }

    public ActionKeyIndex getKeyForAction(String action){
        ActionKeyIndex ret = actionToKey.get(action);
        if(ret == null){
            ConsoleCampaignListener.enqueueForLogging("No configured key for action: '"+action+"'", Color.yellow);
        }

        return ret;
    }
}
