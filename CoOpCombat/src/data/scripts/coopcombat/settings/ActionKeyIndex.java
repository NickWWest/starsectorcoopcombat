package data.scripts.coopcombat.settings;

//A mapping of configured action-key-index where index is the Keyboard key to check
public class ActionKeyIndex {
    public final String action;
    public final int index;

    public ActionKeyIndex(String action, int index) {
        this.action = action;
        this.index = index;
    }

    @Override
    public String toString() {
        return "ActionKeyIndex{" +
                "action='" + action + '\'' +
                ", index=" + index +
                '}';
    }
}
