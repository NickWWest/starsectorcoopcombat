package data.scripts.coopcombat.settings;

import com.fs.starfarer.api.Global;
import org.apache.log4j.Level;
import org.json.JSONObject;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

// represents data found in CoOpCombatSettings.json
public class CoOpConfig {

    private static final JSONObject settingsJson;

    static {

        try {
            // main settings file contains general config
            JSONObject settings = Global.getSettings().loadJSON("CoOpCombatSettings.json");
            settingsJson = settings.getJSONObject("CoOpCombat");
        } catch(Throwable t){
            throw new RuntimeException("Error parsing CoOpCombatSettings.json, reason: " + t.getMessage(),  t);
        }
    }

    public static PlayerConfig[] getPlayerConfigs(){
        try {
            // look for specific player configs.  We'll run out of keys to use by player 4 :/
            List<PlayerConfig> playerConfigList = new ArrayList<>();
            for(int playerId=2; playerId<=2; playerId++){ // currently only support two players
                playerConfigList.add(new PlayerConfig(playerId));
            }

            return playerConfigList.toArray(new PlayerConfig[0]);
        } catch(Throwable t){
            throw new RuntimeException("Error parsing CoOpCombatSettings.json, reason: " + t.getMessage(),  t);
        }
    }

    public static int getMaxCycleTargets(){ return ConfigUtil.getLunaInt("MaxCycleTargets"); }
    public static int getMaxCycleTargetRange(){ return ConfigUtil.getLunaInt("MaxCycleTargetRange"); }
    public static float getReticleDotSize(){ return ConfigUtil.getLunaFloat("ReticleDotSize"); }

    public static final Color ReadyColor = ConfigUtil.getColorSetting(settingsJson, "CoOp_ReadyColor", new Color(20,223,73));
    public static final Color AlmostColor = ConfigUtil.getColorSetting(settingsJson, "CoOp_AlmostColor", new Color(251,237,5));
    public static final Color NotReadyColor = ConfigUtil.getColorSetting(settingsJson, "CoOp_NotReadyColor", new Color(255,127,39));
    public static final Color UnavailableColor = ConfigUtil.getColorSetting(settingsJson, "CoOp_UnavailableColor", new Color(251,5,5));
    public static final Color UiColor = ConfigUtil.getColorSetting(settingsJson, "CoOp_UiColor", new Color(155,255,0)); // same color as the green used in the UI
    public static final String LoggingLevel = ConfigUtil.getStringSetting(settingsJson, "CoOp_LoggingLevel", Level.INFO.toString());
}
