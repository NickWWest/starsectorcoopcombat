package data.scripts.coopcombat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ViewportAPI;
import data.scripts.coopcombat.settings.CoOpConfig;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

// taken from the excellent Leading Pip Mod, attribution is here by granted to Magehand LLC per licensing.
public final class AimingReticle {

    private static final float SEGMENT_LEN = 125;

    // length of our line, enough to create a boxy-dot approximation
    // bigger than the standard dot, since second player will often be viewing a stream that is slightly lower quality.
    // 3.3 is the normal dot size
    private static final float DOT_SIZE = CoOpConfig.getReticleDotSize();

    private static final float HALF_DOT_SIZE = DOT_SIZE / 2;

    private static void glColor(Color color, float alphaMult) {
        GL11.glColor4ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue(), (byte) (color.getAlpha() * alphaMult));
    }

    public static void drawAimingReticle(Vector2f start, float distance, float angle, CombatEngineAPI engine, Color color) {

        ViewportAPI viewport = engine.getViewport();

        GL11.glPushAttrib(GL11.GL_ALL_ATTRIB_BITS);
        final int width = (int) (Display.getWidth() * Display.getPixelScaleFactor()), height = (int) (Display.getHeight() * Display.getPixelScaleFactor());
        GL11.glViewport(0, 0, width, height);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();
        GL11.glOrtho(viewport.getLLX(), viewport.getLLX() + viewport.getVisibleWidth(), viewport.getLLY(), viewport.getLLY() + viewport.getVisibleHeight(), -1, 1);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPushMatrix();
        GL11.glLoadIdentity();

        GL11.glDisable(GL11.GL_TEXTURE_2D);
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glEnable(GL11.GL_LINE_SMOOTH);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glTranslatef(0.01f, 0.01f, 0);

        // the line width should be a non-integral value to give better smoothing using the blend function. this value gives pleasantly plump pips.
        // http://stackoverflow.com/questions/3512456/how-to-draw-smooth-line-with-antialias
        // http://stackoverflow.com/a/3533721/787960
        // need to invert our viewport multi to get the behavior we want here, line is defined by the following two points
        //p1 = (.5, 1)
        //p2 = (5, .1)
        float viewportScalar = (1.1f + viewport.getViewMult() * -.2f);
        viewportScalar *= DOT_SIZE;

//        CoOpCombatModPlugin.getLogger().info("width: " + viewportScalar);

        // don't go too small, can't be seen
        GL11.glLineWidth(Math.max(HALF_DOT_SIZE, viewportScalar));

        glColor(color, .45f * (1f - Global.getCombatEngine().getCombatUI().getCommandUIOpacity()));

        // get the angle to draw our pips. 90 degrees is used to offset the aiming pips perpendicular to our aim angle (pip bracketing)
        double angleAsRadians = Math.toRadians(angle);
        float xIdent = (float) Math.cos(angleAsRadians );
        float yIdent = (float) Math.sin(angleAsRadians);

        angleAsRadians = Math.toRadians(angle + 90);
        float xNormalIdent = (float) Math.cos(angleAsRadians );
        float yNormalIdent = (float) Math.sin(angleAsRadians);

        float offsetFromCenter = 5;
        float xOffsetFromCenter = xNormalIdent * offsetFromCenter;
        float yOffsetFromCenter = yNormalIdent * offsetFromCenter;

        int segments = (int)(distance / SEGMENT_LEN);

        // the origins of our aiming pip lines
        Vector2f leftCenterOrig = new Vector2f(start.x + xOffsetFromCenter, start.y + yOffsetFromCenter);
        Vector2f rightCenterOrig = new Vector2f(start.x - xOffsetFromCenter, start.y - yOffsetFromCenter);


        for(int i = 1; i <= segments; i++) {
            // start and end of each pip, left-side. use the heading vector times the segment length as our offset from the origin
            Vector2f leftStart = Vector2f.add(leftCenterOrig, new Vector2f(xIdent*SEGMENT_LEN*i, yIdent*SEGMENT_LEN*i), null);
            Vector2f leftEnd = Vector2f.add(leftStart, new Vector2f(xNormalIdent*DOT_SIZE, yNormalIdent*DOT_SIZE), null);

            // start and end of each pip, right-side. use the heading vector times the segment length as our offset from the origin
            Vector2f rightStart = Vector2f.add(rightCenterOrig, new Vector2f(xIdent*SEGMENT_LEN*i, yIdent*SEGMENT_LEN*i), null);
            Vector2f rightEnd = Vector2f.add(rightStart, new Vector2f(xNormalIdent*DOT_SIZE, yNormalIdent*DOT_SIZE), null);

            GL11.glBegin(GL11.GL_LINES);
            GL11.glVertex2f(leftStart.x, leftStart.y);
            GL11.glVertex2f(leftEnd.x, leftEnd.y);
            GL11.glEnd();

            GL11.glBegin(GL11.GL_LINES);
            GL11.glVertex2f(rightStart.x, rightStart.y);
            GL11.glVertex2f(rightEnd.x, rightEnd.y);
            GL11.glEnd();
        }

        GL11.glDisable(GL11.GL_BLEND);

        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glPopMatrix();
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glPopMatrix();

        GL11.glPopAttrib();
    }
}
