package data.scripts.coopcombat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.combat.entities.Ship;
import data.scripts.coopcombat.settings.ActionKeyIndex;
import data.scripts.coopcombat.settings.CoOpConfig;
import data.scripts.coopcombat.settings.PlayerConfig;
import data.scripts.coopcombat.util.Helpers;
import data.scripts.coopcombat.util.KeyToCommand;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;
import java.util.*;
import java.util.List;

public final class CoOpEveryFrameCombatPlugin extends BaseEveryFrameCombatPlugin {
    private static final int WG_SIZE = 25;
    private static final int W_SIZE = 18;
    private static final int SYSTEM_SIZE = 15;

    private static final Logger log = CoOpCombatModPlugin.getLogger();
    private static long _elapsedTime = 0;
    private static long _frameCount = 0;

    public static void logAndReset(){
        if(_frameCount == 0 || _elapsedTime == 0){
            return;
        }

        log.info("total time in CoOpEveryFrameCombatPlugin: " + _elapsedTime + "ms  FrameCount: " + _frameCount + " Mean Time Per Frame: " + (_elapsedTime / (double) _frameCount) + "ms");
        _elapsedTime = 0;
        _frameCount = 0;

        if(_playerAiData != null) {
            for (PlayerAiData aiData : _playerAiData) {
                if (aiData == null || aiData.currentShip == null || aiData.playerConfig == null) {
                    continue;
                }
                aiData.currentShip.resetDefaultAI(); // defensive

                // save our last used ship for next battle
                setLastUsedShipName(aiData.playerConfig.playerId, aiData.currentShip);
            }
        }

        _playerAiData = new ArrayList<>();
    }

    // what ships the current players are assigned
    private static List<PlayerAiData> _playerAiData = new ArrayList<>();

    private final int _maxCycleTargets = CoOpConfig.getMaxCycleTargets();
    private final int _maxCycleTargetRange = CoOpConfig.getMaxCycleTargetRange();

    private CombatEngineAPI engine;

    @Override
    public void renderInWorldCoords(ViewportAPI viewport) {
        if(engine == null){
            return;
        }

        long start = System.currentTimeMillis();

        try {
            handleShipChange(engine);

            for (PlayerAiData aiData : _playerAiData) {
                CoOpShipAI ai = getCoOpAi(aiData.currentShip);
                if (ai == null) {
                    continue;
                }

                handleCycleTarget(aiData);
                renderCoOpAiSpecific(aiData.currentShip, ai.getWeaponGroupIndex(), aiData.playerConfig.playerColor);
            }
        } catch (Exception e){
            log.error("Error processing CoOpEveryFrameCombatPlugin.renderInWorldCoords  reason: " + e.getMessage(), e);
            Helpers.logCombatErrorMessage("Error while rendering coop combat mod UI, see log for details");
        }

        _elapsedTime += System.currentTimeMillis() - start;
        _frameCount++;
    }

    private static CoOpShipAI getCoOpAi(ShipAPI s){
        if(s == null){
            return null;
        }

        ShipAIPlugin pluginAi = s.getShipAI();

        if(!(pluginAi instanceof Ship.ShipAIWrapper)){
            return null;
        }
        return (CoOpShipAI)((Ship.ShipAIWrapper)s.getShipAI()).getAI();
    }

    //See if the players have requested to use a different ship than the one they currently have
    private void handleShipChange(CombatEngineAPI engine){
        for(PlayerAiData aiData : _playerAiData){
            playerShipSelect(engine, aiData);
        }
    }

    private void playerShipSelect(CombatEngineAPI engine, PlayerAiData aiData){
        // if key is pressed, select the next ship that doesn't have an AI
        if(aiData.selectShipCommand.shouldGiveCommand()) {
            ShipAPI currentShip = aiData.currentShip;
            ShipAPI newShip = getNextShip(getCoOpCapableShips(engine), currentShip);
            aiData.currentShip = newShip;
            // reset the ship we're no longer in command of to its default state
            if(currentShip != null){
                currentShip.resetDefaultAI();
            }

            // enable new AI
            if(newShip != null){
                newShip.setShipAI(new CoOpShipAI(newShip, aiData.playerConfig));
            }

            Global.getSoundPlayer().playUISound("ui_button_pressed",1f, 0.5f);
        }

        // if the player doesn't have a ship, and it's near the beginning of battle, try to give them the last ship they had
        else if(aiData.currentShip == null && aiData.lastUsedShipName != null &&
                engine.getTotalElapsedTime(false) > 1 && engine.getTotalElapsedTime(false) < 2){
            for (ShipAPI s : getCoOpCapableShips(engine)) {

                // compare based on name, it's the only stable thing between combats, and ensure no one else is somehow using it
                if (s.getName().equals(aiData.lastUsedShipName) && getCoOpAi(s) == null){
                    aiData.currentShip = s;
                    s.setShipAI(new CoOpShipAI(s, aiData.playerConfig));
                }
            }
        }
    }

    private List<ShipAPI> getCoOpCapableShips(CombatEngineAPI engine){
        // get all our ships that could have a coop AI assigned to them
        List<ShipAPI> allShips = engine.getShips();
        List<ShipAPI> ships = new ArrayList<>(allShips.size());
        for (ShipAPI s : allShips) {
            // if we ever want PvP back, just need to modify the below logic to allow players to select enemy ships
            // Main player's ship has no AI
            if(s != null && s.getName() != null && !s.isShuttlePod() && s.getId() != null && !s.isFighter() && !s.isDrone() && s.isAlive() && s.getOwner() == 0 && s.getAI() != null){
                ships.add(s);
            }
        }

        // sort ships so as we do this in different frames, the order is the same
        Collections.sort(ships, new Comparator<ShipAPI>() {
            @Override
            public int compare(ShipAPI o1, ShipAPI o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        return ships;
    }

    // gets the next "available" ship in our list of ships, can be null if we're at the end
    private ShipAPI getNextShip(List<ShipAPI> ships, ShipAPI currentShip){
        // get our currently selected ship index
        int playerShipIndex = 0;
        if(currentShip != null) {
            for (; playerShipIndex < ships.size(); playerShipIndex++) {
                ShipAPI current = ships.get(playerShipIndex);
                if (current.getId().equals(currentShip.getId())) {
                    break;
                }
            }
        }

        // our in-use shipIds, so we don't have two players trying to control the same ship
        List<String> inUseShipIds = new ArrayList<>();
        for(PlayerAiData aiData : _playerAiData){
            if(aiData.currentShip == null){
                continue;
            }
            inUseShipIds.add(aiData.currentShip.getId());
        }

        // get the next available ship
        for(; playerShipIndex<ships.size(); playerShipIndex++){
            ShipAPI current = ships.get(playerShipIndex);
            if(inUseShipIds.contains(current.getId())){ // if this ship is already selected by a player
                continue;
            }

            return current;
        }

        return null;
    }

    private void handleCycleTarget(PlayerAiData aiData){
        if(aiData.currentShip == null){
            return;
        }

        boolean next = aiData.nextTargetCommand.shouldGiveCommand();
        boolean prev = aiData.prevTargetCommand.shouldGiveCommand();

        // cycle through which ship is targeted.  Have to do this since there's no mouse to specify.
        if (next || prev) {
            List<ShipDistancePair> allEnemyShips = getEnemyShips(aiData.currentShip); // enemy ships sorted by distance
            List<ShipAPI> nearbyEnemyShips = new ArrayList<>(_maxCycleTargets);

            for (ShipDistancePair sdp : allEnemyShips) {
                // limit to the 10 closest, no one wants to cycle through more than that
                if (nearbyEnemyShips.size() < _maxCycleTargets && sdp.distance < Math.pow(_maxCycleTargetRange, 2)) {
                    nearbyEnemyShips.add(sdp.ship);
                } else {
                    break;
                }
            }

            if (nearbyEnemyShips.size() > 0) {
                Collections.sort(nearbyEnemyShips, new Comparator<ShipAPI>() {
                    public int compare(ShipAPI s1, ShipAPI s2) {
                        return s1.getId().compareTo(s2.getId()); // doesn't matter the order, just needs to be consistent
                    }
                });

                int targetIndex = nearbyEnemyShips.indexOf(aiData.currentShip.getShipTarget());
                if(next) {
                    targetIndex++; // go to next ship
                } else if(prev){
                    targetIndex--;
                }

                if(aiData.currentShip.getShipTarget() == null){ // if we don't have anything set, start at the beginning or end
                    if(next) {
                        aiData.currentShip.setShipTarget(nearbyEnemyShips.get(0));
                    } else if (prev){
                        aiData.currentShip.setShipTarget(nearbyEnemyShips.get(nearbyEnemyShips.size() - 1));
                    }
                } else if (targetIndex >= nearbyEnemyShips.size() || targetIndex < 0) { // at the end, clear target
                    aiData.currentShip.setShipTarget(null);
                } else {
                    aiData.currentShip.setShipTarget(nearbyEnemyShips.get(targetIndex));
                }
            }
        }
    }

    private List<ShipDistancePair> getEnemyShips(ShipAPI ship){ // nearest to furthest
        List<ShipDistancePair> shipsAndDistance = new ArrayList<>(20);
        for(ShipAPI enemyShip : Global.getCombatEngine().getShips()){
            if(enemyShip.getOwner() == ship.getOwner() || !enemyShip.isAlive() || enemyShip.isFighter() || enemyShip.isDrone() || enemyShip.isShuttlePod() || enemyShip.isAlly()){
                continue;
            }

            float distance = Helpers.getDistanceSquared(ship.getLocation(), enemyShip.getLocation());
            shipsAndDistance.add(new ShipDistancePair(enemyShip, distance));
        }

        Collections.sort(shipsAndDistance);

        return shipsAndDistance;
    }

    private void renderCoOpAiSpecific(ShipAPI s, int weaponGroupIndex, Color playerColor){
        Vector2f shipData = new Vector2f(s.getLocation());
        shipData.setX(shipData.x + s.getCollisionRadius()/2 + WG_SIZE/2f);
        shipData.setY(shipData.y - 2 - WG_SIZE/2f);

        // ship info
        renderShipData(s, weaponGroupIndex, shipData);

        // reticle
        renderReticle(s, weaponGroupIndex, playerColor);

        // targeted ship
        if(s.getShipTarget() != null){
            TargetedShip.drawTargetedShip(s.getShipTarget(), engine, playerColor);
        }
    }

    private void renderReticle(ShipAPI s, int weaponGroupIndex, Color playerColor){
        List<WeaponGroupAPI> weaponsGroup = s.getWeaponGroupsCopy();
        if(weaponsGroup == null || weaponGroupIndex >= weaponsGroup.size()){
            return;
        }
        List<WeaponAPI> activeWeapons = weaponsGroup.get(weaponGroupIndex).getWeaponsCopy();

        for(WeaponAPI w : activeWeapons){
            boolean isMissile = w.getType() == WeaponAPI.WeaponType.MISSILE;

            float range = w.getRange();
            if(isMissile){
                range = Math.min(1000, w.getRange());
            }

            // only get aiming pips if you can shoot
            if(!isOutOfAmmo(w) &&
                (frontFacingIsInArc(w, s.getFacing()) || isMissile)) // missiles can 'seek' don't need to point forward. Usually.
            {
                AimingReticle.drawAimingReticle(w.getLocation(), range, s.getFacing(), engine, playerColor);
            }
        }
    }

    private static boolean frontFacingIsInArc(WeaponAPI w, float shipFacing){
        float tolerance = 1.5f;
        float minArc = w.getArcFacing() - w.getArc()/2 - tolerance;
        float maxArc = w.getArcFacing() + w.getArc()/2 + tolerance;

        return 0f > minArc && 0f < maxArc;
    }

    private boolean isOutOfAmmo(WeaponAPI w){
       return w.usesAmmo() && w.getAmmo() == 0 && w.getSpec().getAmmoPerSecond() == 0;
    }

    private void renderShipData(ShipAPI s, int weaponGroupIndex, Vector2f drawPos){
        // system cool down
        renderSystem(s, drawPos);

        float offset = -30 *Global.getCombatEngine().getViewport().getViewMult();
        drawPos.setY(drawPos.y + offset);

        // weapon cool down & group
        renderWeaponCoolDown(s, weaponGroupIndex, drawPos);
    }

    private void renderWeaponCoolDown(ShipAPI s, int weaponGroupIndex, Vector2f drawPos){
        List<WeaponGroupAPI> weaponsGroup = s.getWeaponGroupsCopy();
        if(weaponsGroup == null || weaponGroupIndex >= weaponsGroup.size()){
            return;
        }

        List<WeaponAPI> weapons = weaponsGroup.get(weaponGroupIndex).getWeaponsCopy();
        CooldownRenderer.drawCooldownIndicator(drawPos, -1f, 0, weaponGroupIndex+1, WG_SIZE, false);

        // render a cooldown for each individual weapon
        for(WeaponAPI w : weapons){
            drawPos = new Vector2f(drawPos);
            drawPos.setX(drawPos.x + 1.75f*W_SIZE * Global.getCombatEngine().getViewport().getViewMult());

            int burstsRemaining = -1;
            if(w.usesAmmo()){
                burstsRemaining = w.getAmmo();
                if(w.getSpec().getBurstSize() > 1){
                    burstsRemaining /= w.getSpec().getBurstSize();
                }
            }

            CooldownRenderer.drawCooldownIndicator(drawPos, isOutOfAmmo(w) ? 0f : cooldownPercent(w), 0, burstsRemaining, W_SIZE, w.isDisabled());
        }
    }

    private void renderSystem(ShipAPI ship, Vector2f drawPos){
        Vector2f loc = new Vector2f(drawPos);
        ShipSystemAPI s = ship.getSystem();
        if(s.getAmmo() == Integer.MAX_VALUE) {
            CooldownRenderer.drawCooldownIndicator(loc, cooldownPercent(s.getCooldown(), s.getCooldownRemaining()), 45, -1, SYSTEM_SIZE, false);
        }
        else
        {
            float offset = 2 *SYSTEM_SIZE * Global.getCombatEngine().getViewport().getViewMult();
            int i=0;
            for(; i<s.getAmmo(); i++){
                CooldownRenderer.drawCooldownIndicator(loc, 1, 45, -1, SYSTEM_SIZE, false);
                loc.setX(loc.x + offset);
            }
            for(; i<s.getMaxAmmo(); i++){
                CooldownRenderer.drawCooldownIndicator(loc, 0, 45, -1, SYSTEM_SIZE, false);
                loc.setX(loc.x + offset);
            }
        }
    }

    private static float cooldownPercent(float coolDown, float cooldownRemaining){
        if(cooldownRemaining == 0){
            return 1f;
        }
        else
        {
            return (coolDown  - cooldownRemaining) / coolDown;
        }
    }

    private static float cooldownPercent(WeaponAPI w){
        if(w.isDisabled()){
            return .25f;
        }

        // if we're out of ammo, we can't be ready to fire
        if(w.usesAmmo() && w.getAmmo() == 0 && w.getSpec().getAmmoPerSecond() > 0){
            return .25f;
        } else {
            return cooldownPercent(w.getCooldown(), w.getCooldownRemaining());
        }
    }

    @Override
    public void init(CombatEngineAPI engine) {
        this.engine = engine;
        log.info("CoOpEveryFrameCombatPlugin initialized");
        _elapsedTime = 0;  // logAndReset not guaranteed to have been called from the simulator
        _frameCount = 0;
        _playerAiData = new ArrayList<>();

        try {
            // setup our select ship commands
            for(PlayerConfig playerConfig : CoOpConfig.getPlayerConfigs()){
                ActionKeyIndex selectShipAction = playerConfig.getKeyForAction("SELECT_SHIP");
                KeyToCommand selectShipCommand = new KeyToCommand(selectShipAction.index, true);

                ActionKeyIndex nextTargetAction = playerConfig.getKeyForAction("NEXT_TARGET");
                KeyToCommand nextTargetCommand = new KeyToCommand(nextTargetAction.index, true);

                ActionKeyIndex prevTargetAction = playerConfig.getKeyForAction("PREV_TARGET");
                KeyToCommand prevTargetCommand = new KeyToCommand(prevTargetAction.index, true);

                PlayerAiData aiData = new PlayerAiData(playerConfig, selectShipCommand, nextTargetCommand, prevTargetCommand, getLastUsedShipName(playerConfig.playerId));
                _playerAiData.add(aiData);
                log.info(aiData);
            }
        } catch (Exception e){
            log.error("Error in CoOpEveryFrameCombatPlugin.init  reason: " + e.getMessage(), e);
            Helpers.logCombatErrorMessage("Error while configuring player `SELECT_SHIP` command. See log for details");
        }

        if(_playerAiData.size() == 0){
            Helpers.logCombatErrorMessage("No valid Co-Op configs could be located.  See log for details.");
        }
    }

    private static String getLastUsedShipName(int playerId){
        return (String) Global.getSector().getPersistentData().get("CoOpShipPlayer" + playerId);
    }

    private static void setLastUsedShipName(int playerId, ShipAPI ship){
        String key = "CoOpShipPlayer" + playerId;
        if(ship == null){
            Global.getSector().getPersistentData().remove(key); // no ship at the end, remove it
        } else {
            // use name not Id, Id isn't stable
            Global.getSector().getPersistentData().put("CoOpShipPlayer" + playerId, ship.getName());
        }
    }

    private static class PlayerAiData {
        public final PlayerConfig playerConfig;
        public final KeyToCommand selectShipCommand;
        public final KeyToCommand nextTargetCommand;
        public final KeyToCommand prevTargetCommand;
        public final String lastUsedShipName;
        public ShipAPI currentShip;

        public PlayerAiData(PlayerConfig playerConfig, KeyToCommand selectShipCommand, KeyToCommand nextTargetCommand, KeyToCommand prevTargetCommand, String lastUsedShipName){
            this.playerConfig = playerConfig;
            this.selectShipCommand = selectShipCommand;
            this.nextTargetCommand = nextTargetCommand;
            this.prevTargetCommand = prevTargetCommand;
            this.lastUsedShipName = lastUsedShipName;
        }

        @Override
        public String toString() {
            return "PlayerAiData{" +
                    "playerId=" + playerConfig.playerId +
                    ", selectShipCommand=" + selectShipCommand +
                    ", nextTargetCommand=" + nextTargetCommand +
                    ", prevTargetCommand=" + prevTargetCommand +
                    ", lastUsedShipName='" + lastUsedShipName + '\'' +
                    ", currentShip=" + currentShip +
                    '}';
        }
    }

    private static class ShipDistancePair implements Comparable<ShipDistancePair>{
        public ShipAPI ship;
        public float distance;

        public ShipDistancePair(ShipAPI ship, float distance) {
            this.ship = ship;
            this.distance = distance;
        }

        @Override
        public int compareTo(ShipDistancePair shipDistancePair) {
            return Float.compare(this.distance, shipDistancePair.distance);
        }
    }
}
