package data.scripts.coopcombat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ViewportAPI;
import com.fs.starfarer.api.graphics.SpriteAPI;
import data.scripts.coopcombat.settings.CoOpConfig;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public final class CooldownRenderer {

    // color a white sprite with the colors specified in the config
    public static void drawCooldownIndicator(Vector2f anchor, float percentReady, float angle, int number, int size, boolean isDisabled) {
        ViewportAPI screen = Global.getCombatEngine().getViewport();

        SpriteAPI sprite;
        //todo percent ready means too many things, just pass in the color
        if(percentReady >= .9999999999f){
            sprite = getSprite(CoOpConfig.ReadyColor);
        }
        else if(percentReady >= .75){
            sprite = getSprite(CoOpConfig.AlmostColor);
        }
        else if(percentReady >= 0)
        {
            sprite = getSprite(CoOpConfig.NotReadyColor);
        }
        else if(percentReady == 0)
        {
            sprite = getSprite(CoOpConfig.UnavailableColor);
        }
        else
        {
            sprite = getSprite(CoOpConfig.UiColor);
        }

        if(angle != 0){
            sprite.setAngle(angle);
        }

        float scaledSize = size*screen.getViewMult();
        sprite.setSize(scaledSize, scaledSize);
        sprite.renderAtCenter(anchor.x, anchor.y);

        if(isDisabled){
            sprite = Global.getSettings().getSprite("misc", "CoOp_Disabled");
            sprite.setSize(scaledSize, scaledSize);
            sprite.renderAtCenter(anchor.x, anchor.y);
        } else if(number > 9){
            sprite = Global.getSettings().getSprite("misc", "CoOp_Plus");
            sprite.setSize(scaledSize, scaledSize);
            sprite.renderAtCenter(anchor.x, anchor.y);
        } else if(number > 0){
            sprite = Global.getSettings().getSprite("misc", "CoOp_"+number);
            sprite.setSize(scaledSize, scaledSize);
            sprite.renderAtCenter(anchor.x, anchor.y);
        }
    }

    private static SpriteAPI getSprite(Color c){
        SpriteAPI ret = Global.getSettings().getSprite("misc", "CoOp_White");
        ret.setColor(c);

        return ret;
    }
}
