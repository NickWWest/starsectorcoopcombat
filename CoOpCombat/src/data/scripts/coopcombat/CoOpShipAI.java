package data.scripts.coopcombat;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.*;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.coopcombat.settings.ActionKeyIndex;
import data.scripts.coopcombat.settings.CoOpConfig;
import data.scripts.coopcombat.settings.PlayerConfig;
import data.scripts.coopcombat.util.Helpers;
import data.scripts.coopcombat.util.KeyToCommand;
import org.apache.log4j.Logger;
import org.lwjgl.util.vector.Vector2f;

import java.util.*;
import java.util.List;

/**
 * Allows keyboard inputs to control a ship that isn't the players ship (main player is considered player1).
 * There are a major differences for piloting a coop ship vs the ship player1 has.
 * Those differences, and their respective solutions are:
 *   * No mouse to control where a shield faces - Use an AI to "aim" the shield
 *   * No mouse to control where weapons aim - Weapons can only aim directly forward
 *   * No mouse to select targets for weapons -  A new "cycle targets" command
 *   * No visual indicator of which weapon groups are selected - Remember with your brain
 *   * No visual indicator of cool downs - Spam press what ever action key you want triggered
 */
@SuppressWarnings("ALL")
public final class CoOpShipAI implements ShipAIPlugin
{
    private static final Set<String> COMMANDS_TO_TOGGLE = new HashSet<>(Arrays.asList(
            ShipCommand.SELECT_GROUP.name(),
            ShipCommand.TOGGLE_AUTOFIRE.name(),
            ShipCommand.VENT_FLUX.name(),
            ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK.name(),
            ShipCommand.HOLD_FIRE.name(),
            ShipCommand.USE_SYSTEM.name(),
            ShipCommand.PULL_BACK_FIGHTERS.name(),
            "NEXT_TARGET",
            "PREV_TARGET"
    ));

    private static Logger log = CoOpCombatModPlugin.getLogger();
    private static long _elapsedTime = 0;
    private static long _frameCount = 0;

    public static void logAndReset(){
        if(_elapsedTime > 0) {
            log.info("total time in CoOpShipAI: " + _elapsedTime + "ms  FrameCount: " + _frameCount + " Mean Time Per Frame: " + (_elapsedTime / (double) _frameCount) + "ms");
        }
        _elapsedTime = 0;
        _frameCount = 0;
    }

    // things beyond this distance aren't considered for our calculations that looks for enemies.
    // all distances are squared
    private double distanceToCare = Math.pow(CoOpConfig.getMaxCycleTargetRange(), 2);

    private final ShipAPI ship;

    private final KeyToCommand[] keysToCommand;

    // For these ship commands to work they have to do more than just execute ship.giveCommand, we we check them separately
    private KeyToCommand shield;

    private KeyToCommand system;

    // weapon group selection related members
    private KeyToCommand selectWeaponGroup;
    private int weaponGroupIndex = 0;
    private final boolean[] isWeaponGroupDefaultAutoFire;
    private boolean enterWeaponGroup;

    private KeyToCommand nextTarget;
    private KeyToCommand prevTarget;

    private LinkedList<Integer> autoFireCommandQueue = new LinkedList<>();

    // use a shield AI to keep our shield pointed in the proper direction since we can't use the mouse
    private final boolean useShieldDirectionAi;
    private final IntervalUtil shieldCheckInterval = new IntervalUtil(0.1f, 0.25f);

    public CoOpShipAI(ShipAPI ship, PlayerConfig playerConfig)
    {
        _elapsedTime = 0;  // might not be zero from the simulator
        _frameCount = 0;

        this.ship = ship;

        // build the list of keys the user has defined to check for press
        List<KeyToCommand> keysToCommandList = new ArrayList<>();
        for(ShipCommand sc : ShipCommand.values()){
            if(sc.name().equals("USE_SELECTED_GROUP")){ // don't evaluate deprecated, use string here to avoid compilation warn
                continue;
            }

            // look for all configs in settings.json that are ours
            ActionKeyIndex aki = playerConfig.getKeyForAction(sc.name());
            KeyToCommand ktc = new KeyToCommand(aki.index, sc, COMMANDS_TO_TOGGLE.contains(sc.name()));

            if(sc == ShipCommand.USE_SYSTEM){
                system = ktc;
            } else if(sc == ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK){
                shield = ktc;
            } else if(sc == ShipCommand.SELECT_GROUP){
                selectWeaponGroup = ktc;
            } else {
                keysToCommandList.add(ktc);
            }

            log.info(ktc);
        }
        keysToCommand = keysToCommandList.toArray(new KeyToCommand[0]);

        ActionKeyIndex nextTarget = playerConfig.getKeyForAction("NEXT_TARGET");
        this.nextTarget = new KeyToCommand(nextTarget.index, null, true);
        log.info(String.format("Key: '%1$s'  Command: 'NEXT_TARGET'", nextTarget));

        ActionKeyIndex prevTarget = playerConfig.getKeyForAction("PREV_TARGET");
        this.prevTarget = new KeyToCommand(prevTarget.index, null, true);
        log.info(String.format("Key: '%1$s'  Command: 'PREV_TARGET'", prevTarget));

        this.useShieldDirectionAi = ship.getShield() != null && ship.getShield().getType() == ShieldAPI.ShieldType.OMNI;
        log.info("Using shield direction AI: " + useShieldDirectionAi);

        List<WeaponGroupAPI> wg = ship.getWeaponGroupsCopy();
        isWeaponGroupDefaultAutoFire = new boolean[wg.size()];
        for (int i=0; i<wg.size(); i++){
            isWeaponGroupDefaultAutoFire[i] = wg.get(i).isAutofiring();
        }
        enterWeaponGroup = true; // true so we can activate our group 0

        resetWeaponGroups(ship.getWeaponGroupsCopy()); // defensive
    }

    @Override
    public void advance(float amount)
    {
        if (Global.getCombatEngine() == null) return;
        if (Global.getCombatEngine().isPaused()) return;
        if(ship == null) return;

        try {
            long start = System.currentTimeMillis();

            // NOTE: Can't seem to give commands while game is paused, seems to be a limitation of ShipAPI.giveCommand

            // switching weapons group, we can't disable and enable autofire in different groups in the same frame
            // so break it up into entering & exiting
            List<WeaponGroupAPI> weaponGroupsCopy = ship.getWeaponGroupsCopy();
            if (enterWeaponGroup) {
                enterWeaponGroup(weaponGroupsCopy);
                enterWeaponGroup = false;
            }

            if (selectWeaponGroup != null && selectWeaponGroup.shouldGiveCommand()) {
                resetWeaponGroups(weaponGroupsCopy);
                exitWeaponGroup(weaponGroupsCopy);
                enterWeaponGroup = true;
            }

            // keep the active weapons pointed forward.  If we have weapons.
            // Can't do this via setting the cursor since that messes with the shield facing
            if (weaponGroupsCopy != null && weaponGroupIndex < weaponGroupsCopy.size()) {
                for (WeaponAPI w : weaponGroupsCopy.get(weaponGroupIndex).getWeaponsCopy()) {
                    w.setCurrAngle(ship.getFacing());
                }
            }

            Vector2f inFront = getLocationInFront(1000, ship.getLocation(), ship.getFacing());

            shieldCheckInterval.advance(amount);

            // each frame, check all keys that are defined, see if they are pressed, if so perform that action.
            // this is everything not explicitly handled below
            for (KeyToCommand ktc : keysToCommand) {
                if (ktc.shouldGiveCommand()) {
                    ship.giveCommand(ktc.shipCommand, inFront, weaponGroupIndex);
                }
            }

            // watch for system activation
            if (system != null && system.shouldGiveCommand()) {
                ship.useSystem();
            }

            // shields or phase
            if (shield != null && shield.shouldGiveCommand()) {
                ship.giveCommand(ShipCommand.TOGGLE_SHIELD_OR_PHASE_CLOAK, null, 0);
            }

            // aim our shield
            if (useShieldDirectionAi && ship.getShield().isOn() && shieldCheckInterval.intervalElapsed()) {
                // get nearest threat
                Vector2f threatLocation = getNearestPrimaryThreat(ship.getLocation());

                // set shield facing
                if (threatLocation == null) {
                    ship.getMouseTarget().set(inFront.getX(), inFront.getY());
                } else {
                    ship.getMouseTarget().set(threatLocation.getX(), threatLocation.getY());
                }
            }

            executeNextAutoFireCommand();

            _elapsedTime += System.currentTimeMillis() - start;
            _frameCount++;
        } catch(Throwable t) {
            log.error("Error processing frame", t);
            Helpers.logCombatErrorMessage("Error in coop combat mod, please look for exception in log and post to forum");
        }
    }

    // reset the now old weapon group to it's default auto-fire state
    // this should be done before altering auto-fire state based on issue described here: http://fractalsoftworks.com/forum/index.php?topic=11598.msg273015#msg273015
    // reset them all, not just the one we're messing with out of defensiveness
    private void resetWeaponGroups(List<WeaponGroupAPI> weaponGroupsCopy){
        if(weaponGroupsCopy == null){
            return;
        }

        for(int i=0; i<weaponGroupsCopy.size(); i++){
            if(weaponGroupsCopy.get(i).isAutofiring() != isWeaponGroupDefaultAutoFire[i]){
                addAutoFireCommandQueue(i);
            }
        }
    }

    // Can only execute one auto-fire command per frame, so we have a queue to manage it
    private void addAutoFireCommandQueue(int i){
        if(!autoFireCommandQueue.contains(i)) { // no dupes, would cause thrashing
            autoFireCommandQueue.addLast(i);
        }
    }

    private void executeNextAutoFireCommand(){
        if(autoFireCommandQueue.size() > 0) {
            int group = autoFireCommandQueue.removeFirst();
            ship.giveCommand(ShipCommand.TOGGLE_AUTOFIRE, null, group);
        }
    }

    private void exitWeaponGroup(List<WeaponGroupAPI> weaponGroupsCopy){
        if(weaponGroupsCopy == null || weaponGroupIndex >= weaponGroupsCopy.size()){
            return;
        }

        weaponGroupIndex++;
        if(weaponGroupIndex >= weaponGroupsCopy.size()){
            weaponGroupIndex = 0;
        }
        ship.giveCommand(selectWeaponGroup.shipCommand, null, weaponGroupIndex);

        log.debug("PRESSED: " + selectWeaponGroup + " Current active weapon group is: " + weaponGroupIndex + " (indexed from 0)");
    }

    private void enterWeaponGroup(List<WeaponGroupAPI> weaponGroupsCopy){
        if(weaponGroupsCopy == null || weaponGroupIndex >= weaponGroupsCopy.size()){
            return;
        }

        // when we select a new weapons group, we won't be able to fire the guns if auto-fire is enabled
        // so we need to disable it if it's enabled, but then when we leave the group, we need to reset it to its last state
        WeaponGroupAPI wg = weaponGroupsCopy.get(weaponGroupIndex);
        if(wg.isAutofiring()){
            addAutoFireCommandQueue(weaponGroupIndex);
        }

        for(WeaponAPI w : wg.getWeaponsCopy()){
            w.beginSelectionFlash();
        }
    }

    public int getWeaponGroupIndex(){
        return weaponGroupIndex;
    }

    //Can be called outside the AI
    @Override
    public void setDoNotFireDelay(float amount) {
    }

    //Only called when Player puts ship back under AI control
    @Override
    public void forceCircumstanceEvaluation() {
    }

    //Only gets set via other scripts here, but can be caught by other AI scripts (carriers or healing drones, for example).
    @Override
    public boolean needsRefit() {
        return false;
    }

	@Override
	public ShipwideAIFlags getAIFlags() {
		return new ShipwideAIFlags();
	}

    @Override
    public void cancelCurrentManeuver() {
    }

    @Override
    public ShipAIConfig getConfig() {
        ShipAIConfig ret = new ShipAIConfig();
        return ret;
    }

    public static Vector2f getLocationInFront(float distance, Vector2f location, double angle){
        double angleAsRadians = Math.toRadians(angle);
//        log.debug("Location: " +location+ "Angle: " + angle + "  as radians: " + angleAsRadians);
	    // if this turns into a perf bottleneck, compute every few frames, cache the answer.
        return new Vector2f(location.x + (float)(distance * Math.cos(angleAsRadians)), location.y + (float)(distance * Math.sin(angleAsRadians)));
    }

    // you could really go down the rabbit hole here and get into coding up an "AI"
    private Vector2f getNearestPrimaryThreat(Vector2f shipLocation){
        // threat could be a missile or ship, but for now we are super naive and just go with nearestShip ship
        ShipAPI nearestShip = null;
        float nearestShipDistance = Float.MAX_VALUE;

        ShipAPI nearestFighter = null;
        float nearestFighterDistance = Float.MAX_VALUE;

        for(ShipAPI ship : Global.getCombatEngine().getShips()){
            if(ship.getOwner() == this.ship.getOwner() || !ship.isAlive() || ship.getId().equals(this.ship.getId())){
                continue;
            }

            float distance = Helpers.getDistanceSquared(shipLocation, ship.getLocation());

            if(ship.isFighter() || ship.isDrone()){
                if(distance < nearestFighterDistance){
                    nearestFighter = ship;
                    nearestFighterDistance = distance;
                }
            }
            else
            {
                if(distance < nearestShipDistance){
                    nearestShip = ship;
                    nearestShipDistance = distance;
                }
            }
        }

        if(nearestShipDistance > distanceToCare){
            nearestShip = null;
        }

        // no ships, use a fighter.
        if(nearestShip == null){
            nearestShip = nearestFighter;
        }

        if(nearestShip == null){
            return null;
        }

        return nearestShip.getLocation();
    }
}