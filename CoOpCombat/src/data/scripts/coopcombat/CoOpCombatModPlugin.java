package data.scripts.coopcombat;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.combat.*;
import data.scripts.coopcombat.settings.CoOpConfig;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public final class CoOpCombatModPlugin extends BaseModPlugin {
    private static final Logger log = Global.getLogger(CoOpCombatModPlugin.class);

    static{
        Level loggingLevel;
        try {
            loggingLevel = Level.toLevel(CoOpConfig.LoggingLevel, Level.INFO);
        } catch (Throwable ignored){
            loggingLevel = Level.INFO;
        }
        log.setLevel(loggingLevel);
        log.info("CoOpCombatModPlugin referenced - Logging level: " + loggingLevel);
    }

    public static Logger getLogger(){
        return log;
    }

    @Override
    public void onGameLoad(boolean newGame) {
        // transient so on mod disabled we aren't referenced
        // register our listener that gets notified when battles complete
        Global.getSector().addTransientListener(new ReportPlayerEngagementCampaignEventListener());
        Global.getSector().addTransientScript(new ConsoleCampaignListener());
    }

    private static class ReportPlayerEngagementCampaignEventListener extends BaseCampaignEventListener{
        public ReportPlayerEngagementCampaignEventListener(){
            super(false);
        }

        @Override
        public void reportBattleOccurred(CampaignFleetAPI primaryWinner, BattleAPI battle){
            clearState();
        }

        @Override
        public void reportBattleFinished(CampaignFleetAPI primaryWinner, BattleAPI battle){
            clearState();
        }

        @Override
        public void reportPlayerEngagement(EngagementResultAPI result) {
            clearState();
        }

        private void clearState(){
            log.debug("State Cleared");

            // log performance stats
            CoOpShipAI.logAndReset();
            CoOpEveryFrameCombatPlugin.logAndReset();
        }
    }
}
